---
id: 7snkcxdk3mbb1x86qguk2cb
title: Python
desc: ''
updated: 1658557120431
created: 1658463553440
---

# Terms
## Basics
1. **Program**
~ A set of instruction that a computer uses to perform a specific action.
2. **Variable**
~ a place in computer memory with an assigned name. We need varialbes to store and retrieve data of different types. (text, numbers, etc.)
3. **List**
~ A data type that can store a collection of values
    1. to_do_list=['meet Anna', 'buy milk']
4. **Dictionary**
~ A data type that stores an unordered collection of data values. it's the only data type that consists of key-value pairs, not single values. 
    1. example of key value pair:
        "key":"value"
    2. example of Python dictionary:
        capitals = {"Czechia": "Prague", "Lithuania": "Vilnius"}
5. **Function**
~ A reusable block of code that performs a certain action (or actions)
    1. def function_name(parameter):
       your code 
6. **Interger(int)**
~ Whole number
7. **String(str)**
~ Text
8. **Float(float)**
~ Decimal number
9. **Boolean(bool)**
~ True/False
10. **List(list)**
~ A "container" that can store any kind of values. You can create a list with square brackets e.g. [1,2,3,'a','b','c']
11. **Tuple(tuple)**
~ A similar "container" as list with a difference that you cannot update the values in a tuple. You can create a tuple with parentheses (1,2,3,'a','b','c').
12. **Index**
~ number is the location of specific value stored in Python lists or tuples. The first index value of list is always 0.
13. **Script**
~ is a dedicated document for writing Python code that you can execute. Python script files should always have the ''.py'' file extension.
14. **Parsing**
~the process of converting codes to machine language to analyze the correct syntax of the code

15. **API** (application programming interface)
~ a set of functions and procedures allowing the creation of applications that access the features or data of an operationg system, application, or other service
    1. examples: OpenWeather.org or OpenNotify.org

## Commands
1. **print()**
~ Basic Python function, literally prints the result on the screen.
    1. print("HelloWorld")
2. **input()**
~ Basic Python function, gets input from the user by showing a prompt. Data is then stored in string format. 
    1. input("what is your favorite color?")
3. **If Statements**
~ common control structure in Python, enables the compiler to decide if the given condition is met and to execute one action or another.
    1. if <condition>:
          <statement>
    2. x=3
       if x < 5:
          print("x is very small")
4. **For loop / While Loop**
~ Allows us to iterate over sequences like lists, dictionaries, sets, tuples, etc. Let you execute the same code for every element of a collection, which save a lot of time and writing. Also keeps code cleaner.
    1. for loops:
       to_do_list = ['meet Anna', 'buy milk', 'finish project'] 
       for task in to_do_list:
            print(task)

        meet Anna
        buy milk
        finish project

    2. maxValue = 10
       i = 1
       while i < maxValue:
            print(i)
            i += 1      

5. **break**
~ Stops a loop even if the condition is true
    1. i = 1 
       while i < 6:
          print(i)
          if i == 3
            break
    

## Modules
1. **random**
~ This module implements pseudo-random generators for various distributions.
    1. https://docs.python.org/3/library/random.html

2. **requests**
~ Allows you to send HTTP requests using Python.
    1. To install:
    Navigate your command line to the location of PIP, and type the following:
        a. C:\Users\Your Name\AppData\Local\Programs\Python\Python36-32\Scripts>pip install requests 







