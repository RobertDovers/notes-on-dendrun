---
id: veg5f22pwmvwaaotzk1xv8m
title: Gitlab
desc: ''
updated: 1658609402622
created: 1658558606018
---
## Overview
- A Cloud service

_**"You gotta git, before you can commit" - Stephanie Burton**_

- GitLab is a source code management system

- **Git**
~ Version control system, to locally track changes in you project/folder and push & pull changes from remote repositories like Github, BitBucket, GitLab

- **GitLab, GitHub, and BitBucket**
~ Services that allow you to host your projectson a remote repositories, with additional features to help in SDLC (Software Development Lifecycle), CI,CD (continuous integration and continuous deployment or continuous delivery). EX. ( managing, sharing, wiki, bug tracking, etc.)
    - **CI,CD** ~ continuously release code changes to the end environment 


- Biggest advantage of GitLab is the Source Code Management (allows CI,CD and code managemment to be in the same place)

## GitLab Architecture
``` mermaid
classDiagram
GitLab --> Runner1: Job 1
GitLab --> Runner2: Job 2
Runner1: server1
Runner2: server2
GitLab : GitLab Instance
GitLab : or
GitLab : GitLab Server
GitLab : Saas(Maintained  by GitLab)
```
- GitLab jobs are ran off of pipline runners
- Pipline runners can be installed on different environment(windows, linex, etc.) depending on the **executer**
    - shell is the simplest executer
    - docker is a common executor for gitlab
        - docker containers run the jobs


## Getting started 
1. Running test 
    1. verifing any changes made did not breat the code
    2. 

### The Pipeline

- Pipline is written in code
    - hosted inside application's git repository 
- Whole CI,CD configuration is writtion in YAML format
    ex. **.gitlab-ci.yml**
### Git Commands
1. **git** --version
~ shows git version
2. **git init**
    1. initializes the current directory as a git repository without any paraments
    2. can do **git init example-text** to creat a sub directory as the git repository
3. **git config --global init.defaultbranch.main**
4. **git branch -m main**
5. ** git status**
~ give the current state of the git repository

