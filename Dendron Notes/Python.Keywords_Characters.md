---
id: j8yi8cy0jsxu2p860ug9oia
title: Keywords&Characters
desc: ''
updated: 1658558549714
created: 1658524489045
---

## Keywords
![Python Keywords](2022-07-22-16-54-29.png)

1. **\\**
~ Escape character, allows you to use illegal characters. 
Ex. 'We\'re the best'
    1. \n ~ New line character 
    2. \r ~ Carriage return character
    3. \t ~ tab character
    4. \b ~ backspace character
    5. \f ~ form feed character
    6. \ooo ~ octal value character
    7. \xhh ~ Hex value character
### Assignment Operators
1.  **=** 
~ Assign
2. **+=**
~ add and assign
3. **-=**
~ subtract and assign
4. **/=**
~ divide and assign
5. ***=**
~ multiply and assign
6. **%=**
~ modeulus and assign
7. ****=**
~ exponent and assign 
8. **//=**
~ floor-divide and assign

### Relational Operators
1. == ~ equal too
2. <= ~ less than or equal too
3. \>= ~ greater than or equal too
4. != ~ not equal too
5. **<** ~ less than
6. **\>** ~ greater than 

### Arthemetic Operators 
1. **-**
~ subtraction
2. **+**
~ addition.
3. **\***
~ multiplication
4. ** ~ exponentiation, raised the first number to the power of the second.
5. **/**
~ division
    1. // ~ floor division, Divides and returns the integer value of the quotient. It dumps the digits after the decimal.
6. **%** ~ modulus, Divides and returns the value of the remainder.


